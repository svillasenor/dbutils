/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbUtils;

import cat.teknos.m06.dbUtils.exceptions.DbUtilsException;
import cat.teknos.m06.dbUtils.exceptions.InvalidSourceException;
import java.io.File;

/**
 *
 * @author Sergi
 */
public class SchemaLoaderImpl implements SchemaLoader{

    /**
     *
     * @param path
     * @throws InvalidSourceException
     */
    @Override
    public void load(String path) throws InvalidSourceException{
        if (path != null && path != "" && path.substring(path.length()-4, path.length()) == ".sql"){
            File file = new File(path);
            
            if (file.exists() && !file.isDirectory()){
            
            }
            else{
                throw new InvalidSourceException();
            }
            
        }
        else{
            throw new DbUtilsException();
        }
        
    }
    
}
