/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbUtils;

import cat.teknos.m06.dbUtils.exceptions.InvalidSourceException;

/**
 *
 * @author Sergi
 */
public interface FileUtils {
    public void toUTF8(ValidCharset charset, String from, String to) throws InvalidSourceException;
}
