/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbUtils;

import cat.teknos.m06.dbUtils.exceptions.InvalidSourceException;

/**
 *
 * @author Sergi
 * throws InvalidSourceException if the file doesn't exist or it's a folder
 * throws DBUtilsExceptionif path is null, empty, or path doesn't end with .sql
 */
public interface SchemaLoader {
    void load(String path) throws InvalidSourceException;
}
