/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbUtils;

import cat.teknos.m06.dbUtils.exceptions.InvalidSourceException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

/**
 *
 * @author Sergi
 */
public class FIleUtilsImpl implements FileUtils{

    @Override
    public void toUTF8(ValidCharset validCharset, String from, String to) throws InvalidSourceException {
        if (from == null || to == null) {
            throw new InvalidSourceException("The from string is null");
        }
        else {
            File fileFrom = new File(from);
            File fileTo = new File(from);
            if (fileFrom.isFile()) {
                var charset = getCharset(validCharset);

                try ( var reader = new BufferedReader(new FileReader(fileFrom, charset))) {
                    String line;
                    var bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileTo), Charset.forName("UTF8")));
                    while ((line = reader.readLine()) != null) {
                        bw.write(line);
                        System.out.println(line);
                        System.out.println("Aqui");
                    }
                    bw.close();
                    reader.close();
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else {
                throw new InvalidSourceException();
            }
        }

    }
    private static Charset getCharset (ValidCharset validCharset){
        Charset charset;
        
        switch (validCharset){
            case ASCII:
                charset = Charset.forName("US-ASCII");
                break;
            case ISO8859:
                charset = Charset.forName("ISO-8859-1");
                break;
            case UTF8:
                charset = Charset.forName("UTF8");
                break;
            default:
                charset =  Charset.forName("UTF16");
                break;
        }
        return charset;
    }
        
        
}

    

    
    

