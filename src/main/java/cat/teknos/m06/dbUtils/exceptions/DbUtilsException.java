/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbUtils.exceptions;

/**
 *
 * @author Sergi
 */
public class DbUtilsException extends RuntimeException{

    public DbUtilsException() {
    }

    public DbUtilsException(String message) {
        super(message);
    }

    public DbUtilsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbUtilsException(Throwable cause) {
        super(cause);
    }

    public DbUtilsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
    
}
