/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbUtils;

import cat.teknos.m06.dbUtils.exceptions.InvalidSourceException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Sergi
 */
public class FIleUtilsImplTest {
    
    public FIleUtilsImplTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of toUTF8 method, of class FIleUtilsImpl.
     */
    @Test
    public void WhenFromIsNotAFileThenThrowInvalidSourceException() throws Exception {
        Assertions.assertThrows(InvalidSourceException.class, () -> {
            FIleUtilsImpl fileUtils = new FIleUtilsImpl();
            fileUtils.toUTF8(ValidCharset.ISO8859, "C:", "C:\\Users\\Sergi\\To.txt");
        });
    }
    @Test
    public void WhenFromIsNullThenThrowInvalidSourceException() throws Exception {
        Assertions.assertThrows(InvalidSourceException.class, () -> {
            FIleUtilsImpl fileUtils = new FIleUtilsImpl();
            fileUtils.toUTF8(ValidCharset.ISO8859, null, "C:\\Users\\Sergi\\To.txt");
        });
    }
    @Test
    public void WhenToIsNullThenThrowInvalidSourceException() throws Exception {
        Assertions.assertThrows(InvalidSourceException.class, () -> {
            FIleUtilsImpl fileUtils = new FIleUtilsImpl();
            fileUtils.toUTF8(ValidCharset.ISO8859, "C:\\Test.txt", null);
        });
    }
    @Test
    public void WhenToIsADirectoryThenThrowInvalidSourceException() throws Exception {
        Assertions.assertThrows(InvalidSourceException.class, () -> {
            FIleUtilsImpl fileUtils = new FIleUtilsImpl();
            fileUtils.toUTF8(ValidCharset.ISO8859, "C:\\", "C:\\Users\\Sergi");
        });        
    }
    

}
